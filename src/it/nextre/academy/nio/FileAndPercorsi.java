package it.nextre.academy.nio;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

public class FileAndPercorsi {
    public static void main(String[] args) {
        //paths();
        try {
            files();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void paths() {
        Path abs1 = Paths.get("C:\\Users\\NextreNextre\\Desktop\\paths.txt");
        Path abs2 = Paths.get("paths.txt");
        System.out.println(abs1);
        System.out.println(abs2);
        System.out.println("\n" + abs1.toAbsolutePath());
        System.out.println(abs2.toAbsolutePath());

        System.out.println('\n' + "--------------------------------------" + '\n');

        Path rel1 = Paths.get("C:\\Users\\NextreNextre", "Desktop\\paths.txt");
        Path rel2 = Paths.get("C:\\Users\\NextreNextre\\Projects", "Esempi teoria\\paths.txt");
        System.out.println(rel1);
        System.out.println(rel2);
        System.out.println(rel1.toAbsolutePath());
        System.out.println(rel2.toAbsolutePath());

        System.out.println('\n' + "--------------------------------------" + '\n');

        System.out.println(rel1.getFileName());
        System.out.println(abs2.getFileName());

        System.out.println('\n' + "--------------------------------------" + '\n');


        System.out.println(abs1.getName(0));
        System.out.println(abs2.toAbsolutePath().getName(1));
        System.out.println(rel1.getName(1));
        System.out.println(rel2.getName(2));
        System.out.println(abs1.getNameCount());
        System.out.println(abs2.getNameCount());
        System.out.println(rel1.getNameCount());
        System.out.println(rel2.getNameCount());


        System.out.println('\n' + "--------------------------------------" + '\n');

        System.out.println(abs2.getRoot());
        System.out.println(rel1.getRoot());
        System.out.println(abs2.getParent());
        System.out.println(abs1.getParent());
        System.out.println(rel2.getParent());
        System.out.println(abs2.toUri());

        System.out.println('\n' + "--------------------------------------" + '\n');
        //da file abs1 a raggiungere rel2
        System.out.println(abs1.relativize(rel2));
        //e viceversa
        System.out.println(rel2.relativize(rel1));
    }

    public static void files() throws IOException {
        Path path = Paths.get("C:\\Users\\NextreNextre\\Projects\\Esempi teoria\\files.txt");
        boolean fileExists = Files.exists(path);
        if (fileExists) {
            System.out.println("Il file esiste già!");
            //Files.createFile(path);
        } else {
            System.out.println("Creo il file...");
            Files.createFile(path);
            System.out.println("File creato!");
        }

        Path destinationPath = Paths.get("C:\\Users\\NextreNextre\\Projects\\Esempi teoria\\src\\new-src-files.txt");
        //Files.copy(path, destinationPath);
        Files.copy(path, destinationPath, StandardCopyOption.REPLACE_EXISTING);

        destinationPath = Paths.get("C:\\Users\\NextreNextre\\Projects\\Esempi teoria\\new-files.txt");
        Files.move(path, destinationPath, StandardCopyOption.REPLACE_EXISTING);
        Files.delete(destinationPath);

        List<Byte> bytes = new ArrayList<>();
        bytes.add((byte) 13);   //newline = 10
        bytes.add((byte) 10);
        bytes.add((byte) 99);   //'c' = 99
        bytes.add((byte) 99);
        bytes.add((byte) 99);
        bytes.add((byte) 99);
        bytes.add((byte) 99);
        bytes.add((byte) 99);
        Byte[] bs = bytes.toArray(new Byte[0]);
        byte[] byteArray = new byte[bytes.size()];
        for (int i = 0; i < byteArray.length; i++) {
            byteArray[i] = bytes.get(i);
        }
        Files.write(Paths.get("nio-data.txt"), byteArray, StandardOpenOption.APPEND);

        byteArray = Files.readAllBytes(Paths.get("nio-data.txt"));
        for (byte b : byteArray) {
            System.out.print((char) b);
        }


        System.out.println();


        path = Paths.get("C:\\Users\\NextreNextre\\Projects\\Esempi teoria\\src");
        Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                System.out.println("pre visit dir: " + dir);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                System.out.println("visit file: " + file);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                System.out.println("visit file failed: " + file);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                System.out.println("post visit dir: " + dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }
}   //end class
