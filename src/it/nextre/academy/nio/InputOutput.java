package it.nextre.academy.nio;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class InputOutput {
    public static void main(String[] args) throws IOException {

        RandomAccessFile aFile = new RandomAccessFile("nio-data.txt", "rw");
        FileChannel inChannel = aFile.getChannel();

        //create buffer with capacity of 48 bytes
        ByteBuffer buf = ByteBuffer.allocate(48);

        //read into buffer = leggere dal channel
        //e scrivere nel buffer, unico posto
        //possibile per memorizzare ciò che viene letto.
        int bytesRead = inChannel.read(buf);

        while (bytesRead != -1) {

            System.out.println("while");

            buf.flip();  //make buffer ready for read

            while(buf.hasRemaining()){
                System.out.print((char) buf.get()); // read 1 byte at a time
            }
            System.out.println();

            buf.clear(); //make buffer ready for writing
            buf.clear();
            bytesRead = inChannel.read(buf);

        }
        //System.out.println((char) 6);
        //System.out.println((char) 7);

        buf.position(0);
        buf.put((byte) 98);         //'b' = 98
        buf.put((byte) 98);
        buf.put((byte) 98);
        buf.put((byte) 98);
        buf.put((byte) 98);
        buf.put((byte) 98);
        buf.put((byte) 98);
        buf.put((byte) 98);


        //Simulare la scrittura di 5 caratteri alla volta
        //partendo da una stringa casuale lunga 13 caratteri.
        //Ripetere per altre 19 stringhe random.
        //Tra una scrittura e l'altra mettere uno sleep di 1 secondo.

        //Realizzare un programma in grado di splittare un file
        //ad una grandezza in byte inserita dall'utente

        buf.flip();
        System.out.println(buf.limit());
        buf.flip();
        System.out.println(buf.limit());
        buf.flip();
        System.out.println(buf.limit() + "\n");
        int bytesWritten = inChannel.write(buf);
        System.out.println("\n" + bytesWritten);

        /*long size = inChannel.size();
        System.out.println(size);
        inChannel.truncate(size - 2);
        System.out.println(inChannel.size());*/


        inChannel.close();

        aFile.close();

    }

}   //end class
