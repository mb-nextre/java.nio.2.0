package myUtil;

import java.util.Scanner;

public class Util {

    public static int readInteger() {
        System.out.print("> ");
        Scanner keyboard = new Scanner(System.in);
        String input = keyboard.nextLine();

        if (input.equals("")) {
            System.out.println("Errore: inserisci un intero.");
            return readInteger();
        }

        char[] chars = input.trim().toCharArray();

        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];
            boolean isDigit = (c == '0' || c == '1' || c == '2' || c == '3' || c == '4'
                    || c == '5' || c == '6' || c == '7' || c == '8' || c == '9');
            if (i == 0) {
                /*
                E' importante controllare il primo carattere
                perchè potrebbe essere un segno (+ o -)
                 */
                if (!(isDigit || c == '+' || c == '-')) {
                    System.out.println("Errore: inserisci un intero.");
                    return readInteger();
                }
            } else {
                if (!isDigit) {
                    System.out.println("Errore: inserisci un intero.");
                    return readInteger();
                }
            }
        }
        return Integer.parseInt(input.trim());
    }

    public static int readPositiveInteger() {
        Scanner keyboard = new Scanner(System.in);
        String input = keyboard.nextLine();

        if (input.equals("")) {
            System.out.println("Errore: inserisci un intero positivo.");
            return readPositiveInteger();
        }

        char[] chars = input.trim().toCharArray();

        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];
            boolean isDigit = (c == '0' || c == '1' || c == '2' || c == '3' || c == '4'
                    || c == '5' || c == '6' || c == '7' || c == '8' || c == '9');
            if (i == 0) {
                /*
                E' importante controllare il primo carattere
                perchè potrebbe essere un segno +: il segno -
                non viene accettato in quanto non vengono accettati
                numeri negativi
                 */
                if (!(isDigit || c == '+')) {
                    System.out.println("Errore: inserisci un intero positivo.");
                    return readPositiveInteger();
                }
            } else {
                if (!isDigit) {
                    System.out.println("Errore: inserisci un intero positivo.");
                    return readPositiveInteger();
                }
            }
        }
        return Integer.parseInt(input.trim());
    }


}   //end class
